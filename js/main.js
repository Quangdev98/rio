

$(document).ready(function () {


});

$('#slide-top-main, #slide-customer-comments').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	margin: 0,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1,
		},
	}
});


// fix header 
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#header").css({
			"position": "sticky",
			"top": 0,
		});
		$("#header").addClass('active');
	} else {
		$("#header").css({
			"top": "-190px",
		});
		$("#header").removeClass('active');
	}
	prevScrollpos = currentScrollPos;
}

$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header:not(.header-project-detail-not-scroll)').addClass('active');
	} else {
		$('#header:not(.header-project-detail-not-scroll)').removeClass('active')
	};
});


// conunt 
$('#tab-header-custom').waypoint(function () {
	$(".counter").each(function () {
		var $this = $(this),
			countTo = $this.attr("data-countto");
		countDuration = parseInt($this.attr("data-duration"));
		$({ counter: $this.text() }).animate(
			{
				counter: countTo
			},
			{
				duration: countDuration,
				easing: "linear",
				step: function () {
					$this.text(Math.floor(this.counter));
				},
				complete: function () {
					$this.text(this.counter);
				}
			}
		);
	});
}, {
	offset: '100%'
});



// 
function resizeImage() {
	let arrClass = [
		{ class: 'rezi-avand', number: (298 / 266) },
		{ class: 'resize-service', number: (257 / 302) },
		{ class: 'resize-product', number: (269 / 512) },
		{ class: 'resize-certifi', number: (655 / 505) },
	];
	for (let i = 0; i < arrClass.length; i++) {
		let width = $("." + arrClass[i]['class']).width();
		$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		// console.log(width);
		// console.log(arrClass);
		// console.log(width*arrClass[i]['number']);
	}
}
resizeImage();
$(window).on('resize', function () {
	resizeImage();
});
