$(document).ready(function () {
	$('#menu').mmenu();

	$('.mm-menu').append(`<div id="close-nav"><i class="fal fa-times"></i></div>`)
	$('#toggle-menu').click(function(){
		$('html').attr('class', 'mm-wrapper_opened mm-wrapper_blocking mm-wrapper_background mm-wrapper_opening');
		$('#menu').toggleClass('mm-menu_opened');
		$('#menu').removeAttr('aria-hidden')
	})
});
$(document).on('click', '#close-nav', function(){
	$('html').attr('class', '');
	$('#menu').toggleClass('mm-menu_opened');
	$('#menu').attr('aria-hidden', true);
})